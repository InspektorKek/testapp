//
//  FilmTableViewCell.swift
//  TestApp
//
//  Created by Михаил on 18/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class FilmTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = .disclosureIndicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
