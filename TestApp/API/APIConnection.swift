//
//  APIConnection.swift
//  TestApp
//
//  Created by Михаил on 17/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation

class APIConnection {
	
	static fileprivate let APIKey = "3b4b6d35"
	static fileprivate let APIFormat = "http://www.omdbapi.com/?apikey=%@&t=%@&plot=full"
	
	static fileprivate let queue = DispatchQueue(label: "requests.queue", qos: .utility)
	static fileprivate let mainQueue = DispatchQueue.main
	
	func searchRequest(term: String, closure: @escaping (_ entity: Film?, _ error: Error?) -> ()) {
		let string = String(format: APIConnection.APIFormat, APIConnection.APIKey, term)
		let url = URL(string: string)
		let request = URLRequest(url: url!)
		APIConnection.make(request: request) { entity, error in
			closure(entity, error)
		}
	}
	
	fileprivate class func make(session: URLSession = URLSession.shared, request: URLRequest, closure: @escaping (_ entity: Film?, _ error: Error?) -> ()) {
		let task = session.dataTask(with: request) { data, response, error in
			queue.async {
				guard error == nil else {
					return
				}
				guard let data = data else {
					return
				}
				
				do {
					let decoder = JSONDecoder()
					let data = try decoder.decode(Film.self, from: data)
					mainQueue.async {
						closure(data, nil)
					}
				} catch let error {
					mainQueue.async {
						closure(nil, error)
					}
				}
			}
		}
		
		task.resume()
	}
}
