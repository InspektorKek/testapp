//
//  FilmEntity.swift
//  TestApp
//
//  Created by Михаил on 18/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

struct Response: Codable {
	let films: [Film]
}

struct Film: Codable {
	var title: String
	var country: String
	var poster: String
	var director: String
	var actors: String
	var plot: String
	var ratings: [Ratings]
	
	enum CodingKeys: String, CodingKey {
		case title = "Title"
		case country = "Country"
		case poster = "Poster"
		case director = "Director"
		case actors = "Actors"
		case plot = "Plot"
		case ratings = "Ratings"
	}
}

struct Ratings: Codable {
	var source: String
	var value: String
	
	enum CodingKeys: String, CodingKey {
		case source = "Source"
		case value = "Value"
	}
}
