//
//  DetailViewController.swift
//  TestApp
//
//  Created by Михаил on 18/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
	
	@IBOutlet weak var posterImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var countryLabel: UILabel!
	@IBOutlet weak var directorLabel: UILabel!
	@IBOutlet weak var actorsLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	
	@IBOutlet weak var ratingsView: UIView!
	@IBOutlet weak var imdLabel: UILabel!
	@IBOutlet weak var metacriticLabel: UILabel!
	@IBOutlet weak var rottenTomatoesLabel: UILabel!
	
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	var entity: Film?
	
	fileprivate var image: UIImage? {
		get {
			return posterImageView.image
		}
		
		set {
			posterImageView.image = newValue
			activityIndicator.isHidden = true
			activityIndicator.startAnimating()
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		settingsView()
    }
    
	
	func settingsView() {
		if let entity = entity {
			fetchImage(url: entity.poster)
			titleLabel.text = entity.title
			countryLabel.text = entity.country
			directorLabel.text = entity.director
			actorsLabel.text = entity.actors
			descriptionLabel.text = entity.plot
			
			ratingViewd(ratings: entity.ratings)
		}
	}
	
	fileprivate func fetchImage(url: String) {
		activityIndicator.isHidden = false
		activityIndicator.startAnimating()
		DispatchQueue.global(qos: .utility).async {
			if let url = URL(string: url), let imageData = try? Data(contentsOf: url) {
				DispatchQueue.main.async {
					self.image = UIImage(data: imageData)
				}
			}
			else {
				self.image = nil
			}
		}
	}
	
	fileprivate func ratingViewd(ratings: [Ratings]) {
		switch ratings.count {
		case 1:
			imdLabel.text = ratings[0].value
		case 2:
			imdLabel.text = ratings[0].value
			metacriticLabel.text = ratings[1].value
		case 3:
			imdLabel.text = ratings[0].value
			metacriticLabel.text = ratings[1].value
			rottenTomatoesLabel.text = ratings[2].value
		default:
			ratingsView.isHidden = true
		}
	}
}
