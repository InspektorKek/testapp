//
//  ViewController.swift
//  TestApp
//
//  Created by Михаил on 17/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {
	
	@IBOutlet weak var searchBar: UISearchBar!
	
	let APICall = APIConnection()
	
	var films: [Film] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		searchBar.delegate = self
		searchBar.enablesReturnKeyAutomatically = true
	}
	
	func searchButtonClicedAction(text: String?) {
		searchBar.endEditing(true)
		films = []
		self.tableView.reloadData()
		if let text = text {
			APICall.searchRequest(term: text) { (film, error) in
				if error != nil {
					let alertController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
					
					let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
					}
					alertController.addAction(cancelAction)
					self.present(alertController, animated: true)
				}
				if let film = film {
					self.films.append(film)
					self.tableView.reloadData()
				}
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "detailSegue" {
			if let indexPath = tableView.indexPathForSelectedRow {
				let dvc = segue.destination as! DetailViewController
				dvc.entity = films[indexPath.row]
			}
		}
	}

}

extension SearchViewController {
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return films.count
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FilmTableViewCell
		let film = films[indexPath.row]
		
		cell.titleLabel.text = film.title
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}
}

extension SearchViewController: UISearchBarDelegate {
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchButtonClicedAction(text: searchBar.text?.replacingOccurrences(of: " ", with: ""))
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText.count > 0 {
			searchBar.enablesReturnKeyAutomatically = false
		}
		else {
			searchBar.enablesReturnKeyAutomatically = true
		}
	}
}

